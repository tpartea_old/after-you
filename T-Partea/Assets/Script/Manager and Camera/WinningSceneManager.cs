﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WinningSceneManager : MonoBehaviour {
	public Text text;
	public HighScore hs;

	public InputField input;

	
	private string inputValue;
	private int p1Score;
	private int p2Score;
	// Use this for initialization
	void Start () {
		int p1Loot = PlayerPrefs.GetInt("P1Loot");
		int p2Loot = PlayerPrefs.GetInt ("P2Loot");
		int totalLoot = PlayerPrefs.GetInt("TotalLoot");
		float ratiop1 = PlayerPrefs.GetFloat("RatioP1");
		float ratiop2 = PlayerPrefs.GetFloat("RatioP2");

		p1Score = ((p1Loot + (int)(totalLoot * ratiop1)) * 100);
		p2Score = ((p2Loot + (int)(totalLoot * ratiop2)) * 100);

		if (p1Score > p2Score) {
			text.text = "Congratulation Player 1, you have scored " + p1Score.ToString() + "\n while " +
				"your comrade gained " + p2Score.ToString();
		}
		else if (p1Score < p2Score) {
			text.text = "Congratulation Player 2, you have scored " + p2Score.ToString() + "\n while " +
				"your comrade gained " + p1Score.ToString();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void doneEdit(){
		inputValue = input.text;
		int num = 0;
		if (p1Score > p2Score) {
			num = 1;
		} else if (p2Score > p1Score) {
			num = 2;
		}
		if (num == 1) {
			Debug.Log (p1Score+ " "+ p2Score + " " + inputValue);
			hs.updateHS (p1Score,inputValue.ToString()+ " ");
		} else if (num == 2) {
			hs.updateHS(p2Score,inputValue.ToString ()+ " ");
		}
	}
}
