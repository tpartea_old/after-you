﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LootSystem : MonoBehaviour {
	public int playerNum;
	//1 = p1
	//2 = p2
	//3 = total Loot

	public Text text;

	private int loot;
	// Use this for initialization
	void Start () {
		
        if (PlayerPrefs.GetInt ("playerLoot" + playerNum) == 0)
        {
            PlayerPrefs.SetInt("playerLoot" + playerNum, 5);
        }

        loot = PlayerPrefs.GetInt("playerLoot" + playerNum);
	}
	
	// Update is called once per frame
	void Update () {
		text.text = loot.ToString ();
	}

	public void addLoot(int lootAmt){
		loot += lootAmt;
		PlayerPrefs.SetInt ("playerLoot" + playerNum, loot);
	}
	public void redLoot(int lootAmt){
		loot -= lootAmt;
		PlayerPrefs.SetInt ("playerLoot" + playerNum, loot);
	}
	public int getLoot(){
		return loot;
	}
}
