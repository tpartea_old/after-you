﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {
	[Header("Amount of Second")]
	public int seconds;

	private int currentSec;

	public Image image;
	// Use this for initialization
	void Start () {
		currentSec = PlayerPrefs.GetInt ("Timer");
		if (currentSec == 0) {
			currentSec = seconds;
		}
		InvokeRepeating ("countDownTimer", 1, 1);
	}
	
	// Update is called once per frame
	void Update () {
		image.fillAmount = ((float)currentSec / (float)seconds);
	}

	private void countDownTimer(){
		if (currentSec > 0) {
			currentSec -= 1;
			PlayerPrefs.SetInt ("Timer", currentSec);
		} else {
			Application.LoadLevel("Lose");
		}
	}
}
