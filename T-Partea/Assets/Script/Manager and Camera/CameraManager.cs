﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	[Header ("How much to vertically offset the camera")]
	public float yOffSet;

	[Header ("Vaue for zooms")]
	public float maxZoomOutDis = 20f;
	public float startZoomOn = 8f;
	public float zoomStr = 0.5f;

	//camera's position, mostly so we don't do new Vector every time
	private Vector3 thisObjPos, lockPos;
	private Vector3 p1Pos, p2Pos;
	private Camera cam;
	private float defZoom, lockZoom;
	private bool isLocked;

	void Start ()
	{
		//get Camera section of this object
		//attach this script to camera oviously, I think
		cam = this.GetComponent<Camera> ();
		//save default zoom size
		defZoom = cam.orthographicSize;
	}

	// Update is called once per frame
	void Update () 
	{
		//change this later so we can lock the camera to fighting area and realease it when ever we want to
		TrackPlayer ();

		if (isLocked)
		{
			LockedCamera();
		}
		else
		{
			TrackPlayer();
		}
	}

	//set player position
	public void SetPlayerPos(int player, Vector3 position)
	{
		if (player == 1)
		{
			p1Pos = position;
		}
		else if (player == 2)
		{
			p2Pos = position;
		}
	}

	//attach below 2 method to a trigger box
	public void LockCamera(Vector2 pos, float zoomVal)
	{
		//recieve value for camera lock
		lockPos = pos;
		lockPos.z = -10;
		lockZoom = zoomVal;
		isLocked = true;

		//maybe get array of object, so when they get destoryed the camera returns to normal
	}

	public void UnlockCam()
	{
		isLocked = false;
	}

	private void LockedCamera()
	{
		transform.position = lockPos;
		cam.orthographicSize = lockZoom;
	}

	private void TrackPlayer()
	{
		//update camera position based on 2 player's position
		thisObjPos = (p1Pos + p2Pos) / 2;
		
		//prevent camera from going over
		if (thisObjPos.x < 0)
		{
			thisObjPos.x = 0;
		}

		Zoom ();

		thisObjPos.z = -10f;
		thisObjPos.y += yOffSet;
		transform.position = thisObjPos;
	}

	private void Zoom()
	{
		//if distance between 2 player is greater than what has been set (startZoomOn)
		if (Vector3.Distance(p1Pos, p2Pos) > startZoomOn)
		{
			//add more value to zoom size based on distance, modified by zoomStr
			cam.orthographicSize = defZoom + (Vector3.Distance(p1Pos, p2Pos) - startZoomOn) * zoomStr;

			//lock if max distance haz been reached
			if (cam.orthographicSize > maxZoomOutDis)
			{
				cam.orthographicSize = maxZoomOutDis;
			}
		}
		else
		{
			//default
			cam.orthographicSize = defZoom;
		}
	}

	public void delete(){
		Destroy (this);
	}
}
