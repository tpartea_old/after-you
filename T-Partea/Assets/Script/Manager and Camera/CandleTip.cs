﻿using UnityEngine;
using System.Collections;

public class CandleTip : MonoBehaviour {
	public Transform tip;
	public float xChange;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Timer", 1, 1);
	}
	
	// Update is called once per frame
	void Update () {

	}
	void Timer(){
		transform.localPosition = new Vector3(transform.localPosition.x + xChange, transform.localPosition.y, transform.localPosition.z);
	}
}
