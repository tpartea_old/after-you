﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof(GameManager))]

public class UIManager : MonoBehaviour {

	[Header ("Drop UI objects here")]
	[Space (5f)]

	//public Text text;
	[Header ("Drop Ratio Text here")]
	public Text P1_dmg;
	public Text P2_dmg;

	[Header ("Drop Loot text object here")]
	public Text lootP1;
	public Text lootP2;

	[Header ("Player UIs")]
	public Image healthbarP1;
	public Image healthbarP2, staminabarP1, staminabarP2;

	void Start()
	{
		/*
		healthbarP1.fillAmount = 1;
		healthbarP2.fillAmount = 1;
		staminabarP1.fillAmount = 1;
		staminabarP2.fillAmount = 1;
		*/

		P1_dmg.text = "50";
		P2_dmg.text = "50";
	}

	//player Stamina Bar Script
	public void staminaP1Ratio(float percentage){
		staminabarP1.fillAmount = percentage;
	}
	public void staminaP2Ratio(float percentage){
		staminabarP2.fillAmount = percentage;
	}
	//end of Player Stamina Bar Script
	
	//player health bar script
	public void healthP1Ratio(float percentage){
		healthbarP1.fillAmount = percentage;
	}
	public void healthP2Ratio(float percentage){
		healthbarP2.fillAmount = percentage;
	}



	//ends of player health bar script

	//insert damage display here

	//insert monster UI manager here
	// it'll need to find & place monster & their UI in a list
}
