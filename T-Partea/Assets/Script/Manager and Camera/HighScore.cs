﻿using UnityEngine;
using System.Collections;

public class HighScore : MonoBehaviour {
	private int holder;
	private string holderName;

	public void updateHS(int score, string name){
		holder = score;
		holderName = name;
		for (int i = 1; i < 11; i++) {
			if (holder > PlayerPrefs.GetInt("HS"+i)){
				Debug.Log ("Saving");
				int temp = PlayerPrefs.GetInt("HS"+i);
				string tempNick = PlayerPrefs.GetString("HSNick" + i);
				PlayerPrefs.SetInt("HS"+i, holder);
				PlayerPrefs.SetString("HSNick"+ i, holderName);
				holderName = tempNick;
				holder = temp;
			}
		}

	}
}
