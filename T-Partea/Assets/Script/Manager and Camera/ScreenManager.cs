﻿using UnityEngine;
using System.Collections;

public class ScreenManager : MonoBehaviour {

	public void KeyPress(string level){
		if (level == "End Game") 
		{
			Application.Quit();
		}
		else
		{
			Application.LoadLevel(level);
		}
	} 

}
