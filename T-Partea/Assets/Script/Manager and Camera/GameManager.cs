﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	[Header("Text to Showcase Ratio")]
	public Text p1;
	public Text p2;
	
	private int damageP1 = 1, damageP2 = 1, maxDmg;
	private int ratioP1, ratioP2;

	/*
	[Header("MonsterHp Ratio")]
	public Image monsterHPbar;
	public Image monsterHPbar2;

	//private monster Hp
	private float imageFill;
	private float imageFill2;
	*/
	/*
	[Header("Player Stamina")]
	public Image staminabarP1;
	public Image staminabarP2;

	//private player Stamina Bar
	private float p1staminaFill;
	private float p2staminaFill;


	[Header("Player HealthBar")]
	public Image healthbarP1;
	public Image healthbarP2;
	
	//private player health Bar
	private float p1healthFill;
	private float p2healthFill;
	*/

	private UIManager uiMan;

	// Use this for initialization
	void Start () 
	{
		uiMan = this.GetComponent<UIManager>();
	}
	
	// Update is called once per frame
	void Update () 
	{

		/*
		//stamina bar player
		staminabarP1.fillAmount = p1staminaFill;
		staminabarP2.fillAmount = p2staminaFill;

		//health bar player
		healthbarP1.fillAmount = p1healthFill;
		healthbarP2.fillAmount = p2healthFill;
		*/
	}

	// ratio script
	private void UpdateRatio()
	{
		maxDmg = damageP1 + damageP2;
		
		ratioP1 = (int)(((float)damageP1 / (float)maxDmg) * 100f);
		ratioP2 = (int)(((float)damageP2 / (float)maxDmg) * 100f);

		//send info to UI manager
		uiMan.P1_dmg.text = ratioP1.ToString ();
		uiMan.P2_dmg.text = ratioP2.ToString ();
	}
	
	public void addP1(int add){
		damageP1 += add;
		UpdateRatio ();
	}
	public void addP2(int add){
		damageP2 += add;
		UpdateRatio ();
	}

	public float calculateRatioP1(){
		return((float)damageP1 / (float)maxDmg);
	}
	public float calculateRatioP2(){
		return((float)damageP2 / (float)maxDmg);
	}
	//end of ratio script

	/*public void monsterHPRatio(float percentage)
	{
		imageFill = percentage;
	}
	*/
}
