﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighScoreManager : MonoBehaviour {

	public Text HS;
	public HighScore Inspector;

	// Use this for initialization
	void Start () {
		
		HS.text = 
			PlayerPrefs.GetString("HSNick1") + "\t" + PlayerPrefs.GetInt("HS1").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick2") + "\t" + PlayerPrefs.GetInt("HS2").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick3") + "\t" + PlayerPrefs.GetInt("HS3").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick4") + "\t" + PlayerPrefs.GetInt("HS4").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick5") + "\t" + PlayerPrefs.GetInt("HS5").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick6") + "\t" + PlayerPrefs.GetInt("HS6").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick7") + "\t" + PlayerPrefs.GetInt("HS7").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick8") + "\t" + PlayerPrefs.GetInt("HS8").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick9") + "\t" + PlayerPrefs.GetInt("HS9").ToString() + "\n" +
				PlayerPrefs.GetString("HSNick10") + "\t" + PlayerPrefs.GetInt("HS10").ToString()
				;
	}

	// Update is called once per frame
	void Update(){


	}

	public void reset(){
		for (int i = 1; i < 11; i++) {
			PlayerPrefs.DeleteKey("HSNick"+i);
			PlayerPrefs.DeleteKey("HS"+i);
		}
	}

}
