﻿using UnityEngine;
using System.Collections;

public class levelManager : MonoBehaviour {
	private int[] scoreArray;
	private string[] scoreNameArray;

	void Start(){
		scoreArray = new int[11];
		scoreNameArray = new string[11];
	}
	public void loadLevel(string name){
		if (name == "exit")
		{
			Application.Quit();
		}
		else
		{
			for(int i = 1; i < 11; i++){
				scoreArray[i] = PlayerPrefs.GetInt("HS"+i);
				scoreNameArray[i] = PlayerPrefs.GetString("HSNick"+i);
			}
			PlayerPrefs.DeleteAll();
			for(int i = 1; i < 11; i++){
				PlayerPrefs.SetInt("HS"+i, scoreArray[i]);
				PlayerPrefs.SetString("HSNick"+i, scoreNameArray[i]);
			}
			Application.LoadLevel (name);
		}
	}
}
