﻿using UnityEngine;
using System.Collections;

public class RespawnManager : MonoBehaviour {

	//prefab list
	public GameObject player1PreFab;
	public GameObject player2PreFab;

	//private deadstate both players
	private bool p1Dead;
	private bool p2Dead;

	//private deadPosition for both players
	private Vector3 p1DeadPos;
	private Vector3 p2DeadPos;

	//loot Manager
	public LootSystem ls1;
	public LootSystem ls2;

	//cost to revive
	public int maxCost;
	public int costToRevive;

	//private number of death
	private int numberOfDeathP1;
	private int numberOfDeathP2;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (p1Dead) {
			if(Input.GetButtonDown("P1_Revive")){
				if(ls1.getLoot() > getCostToRevive(1)){
					p1Dead = false;
					ls1.redLoot(costToRevive);
					Instantiate(player1PreFab,p1DeadPos,Quaternion.identity);
				}
			}
		}
		if (p2Dead) {
			if(Input.GetButtonDown("P2_Revive")){
				if(ls2.getLoot() > getCostToRevive(2)){
					p2Dead = false;
					ls2.redLoot(costToRevive);
					Instantiate(player2PreFab,p2DeadPos,Quaternion.identity);
				}
			}
		}

		if (p1Dead && p2Dead) {
			Application.LoadLevel ("Lose");
		}
	}



	public void setDeadState(int playerNum, bool state, Vector3 deadPos){
		if (playerNum == 1) {
			p1Dead = state;
			p1DeadPos = deadPos;
		} else if(playerNum == 2){
			p2Dead = state;
			p2DeadPos = deadPos;
		}
	}

	private int getCostToRevive(int playerNum){
		if (playerNum == 1) {
			numberOfDeathP1 += 1;
			if(numberOfDeathP1 == 1){
				costToRevive = 1;
				return(costToRevive);
			} else {
				costToRevive = 1 + 2*(numberOfDeathP1 - 1);
				if(costToRevive > maxCost){
					costToRevive = maxCost;
					return(costToRevive);
				}
			}
		}
		if (playerNum == 2) {
			numberOfDeathP2 += 1;
			if(numberOfDeathP2 == 1){
				costToRevive = 1;
				return(costToRevive);
			} else {
				costToRevive = 1 + 2*(numberOfDeathP2 - 1);
				if(costToRevive > maxCost){
					costToRevive = maxCost;
					return(costToRevive);
				}
			}
		}	return 0;
	}
}
