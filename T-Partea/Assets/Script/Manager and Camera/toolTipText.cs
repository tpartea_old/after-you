﻿using UnityEngine;
using System.Collections;

public class toolTipText : MonoBehaviour {

	public GameObject toolTip1;
	public GameObject toolTip2;
	public GameObject toolTip3;
	public GameObject toolTip4;

	// Use this for initialization
	void Start () {
		toolTip1.SetActive (false);
		toolTip2.SetActive (false);
		toolTip3.SetActive (false);
		toolTip4.SetActive (false);
	}

	public void toolTipOn(int buttonNum){
		switch (buttonNum) {
		case 1:
			toolTip1.SetActive(true);
			break;
		case 2:
			toolTip2.SetActive(true);
			break;
		case 3:
			toolTip3.SetActive(true);
			break;
		case 4:
			toolTip4.SetActive(true);
			break;
		}
	}

	public void toolTipOff(int buttonNum){
		switch (buttonNum) {
		case 1:
			toolTip1.SetActive(false);
			break;
		case 2:
			toolTip2.SetActive(false);
			break;
		case 3:
			toolTip3.SetActive(false);
			break;
		case 4:
			toolTip4.SetActive(false);
			break;
		}
	}
}
