﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextChange : MonoBehaviour {
	public GameObject text1;
	public GameObject text2;
	public GameObject text3;
	public string name;

	private bool spaceClicked;
	private float timerClicked;
	private int clickedAmt;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("P1_OK/Attack") || Input.GetButtonDown("P2_OK/Attack")) {
			spaceClicked = true;
			clickedAmt += 1;
		}

		if (spaceClicked) {
			switch(clickedAmt){
			case 0: break;
			case 1: text1.SetActive(false);
				text2.SetActive(true);
				break;
			case 2: text2.SetActive(false);
				text3.SetActive(true);
				break;
			case 3:  changeLevel();
				break;
				//break;
			}
		}
	}

	private void changeLevel(){
		Application.LoadLevel (name);
	}
}
