﻿using UnityEngine;
using System.Collections;

public class CameraOffManager : MonoBehaviour {
	public CameraManager CM;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player1" || other.tag == "Player2") {
			CM.delete();
		}
	}
}
