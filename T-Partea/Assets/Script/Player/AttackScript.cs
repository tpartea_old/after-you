using UnityEngine;
using System.Collections;

public class AttackScript : MonoBehaviour {
	public BoxCollider2D atkColi;

	
	[Header ("canMove P1 / P2")]
	public PlayerController player;

	[Header ("Attack Stamina Regen")]
	public int regen;

	[Header ("Attack delayer")]
	public float atkTimeMax = 3;
	public float atkTimeMin = 1;

	[Header ("Audio Source for attack here")]
	public AudioSource audSouAtk;

	public int playerNum;

	private float curAtkTime;
	private bool canMoveP1, canMoveP2;

    private Animator myAnimator;
	
	//checking if player is attacking or not
	private bool attackingP1, attackingP2;
	private float timerP1, timerP2;

	// Use this for initialization
	void Start () {
		atkColi.enabled = false;
		attackingP1 = false;
		attackingP2 = false;
		canMoveP1 = true;
		canMoveP2 = true;
		timerP1 = 1.5f;
		timerP2 = 1.5f;
	}
	
	// Update is called once per frame
	void Update () {
		if (playerNum == 1) {

			if (Input.GetButton ("P1_OK/Attack")) {
				bool deadState = player.deadState(1);
				if (!attackingP1 && !deadState) {
					canMoveP1 = false;
					player.setMove (canMoveP1);
					//timerP1 += Time.deltaTime;
					player.charAnimAttack("charge");
				}
			}
			if (Input.GetButtonUp ("P1_OK/Attack")) {
				attackingP1 = true;
				atkColi.enabled = true;
				player.setCharge(1,timerP1);

				player.charAnimAttack ("attacking");
				audSouAtk.Play();
				Debug.Log ("Attacking");
				curAtkTime = timerP1;
			}
			if(attackingP1){
				if (curAtkTime > 0) {
					curAtkTime -= Time.deltaTime;
					
				} else {
					attackingP1 = false;
					atkColi.enabled = false;
					canMoveP1 = true;
					player.setMove (true);
					//timerP1 = 0;
					//animation end
					player.charAnimAttack ("else");
					
					Debug.Log ("P1 Attack ends");
				}
			}
		}
			if (playerNum == 2) {
			if (Input.GetButton ("P2_OK/Attack")) {
				bool deadState = player.deadState(2);
				if (!attackingP2 && !deadState) {
					canMoveP2 = false;
					player.setMove (canMoveP2);
					//timerP2 += Time.deltaTime;
					player.charAnimAttack("charge");
					Debug.Log ("Charging");
				}
			}
			if (Input.GetButtonUp ("P2_OK/Attack")) {
				attackingP2 = true;
				atkColi.enabled = true;
				player.setCharge(2,timerP2);
				player.charAnimAttack ("attacking");
				audSouAtk.Play();
				Debug.Log ("Attacking");
				curAtkTime = timerP2;
				
			}
			if(attackingP2){
				if (curAtkTime > 0) {
					curAtkTime -= Time.deltaTime;
					
				} else {
					attackingP2 = false;
					atkColi.enabled = false;
					canMoveP2 = true;
					player.setMove (true);
					//timerP2 = 0;
					//animation end
					player.charAnimAttack ("else");
					
					Debug.Log ("P2 Attack ends");
				}
			}
		}
			
	}
}