﻿using UnityEngine;
using System.Collections;

public class attackTrigger : MonoBehaviour {
	
	public GameManager gameMan;

	[Header ("Player Number")]
	public int playerNum;

	public Transform player;


	void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log("ontriggerenter activated");
		if (other.isTrigger != true) 
		{
			if(other.tag == "Monster" || other.tag == "Boss"){
			other.SendMessageUpwards("playerNumSet", playerNum);
			other.SendMessageUpwards("TakeDamage",player);
			}
		}
	}
}
