using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	[Header ("Player 1 or 2?")]
	public int playerNumber;

    [Header("Player Stat")]
    public int maxHealth;
    public int maxStamina, minDamage, maxDamage;

	[Header ("Speed modifier")]
	public float speed = 1.0f;
	public float sprint = 2.0f;

	[Header ("Cost to Sprint")]
	public int cost;

	[Header ("Stamina Regen")]
	public int regen;

	[Header ("Jump Strength")]
	public float jumpStr;

	//[Header ("Attack delayer")]
	//public float atkTime = 3;
	[Header ("lootSystem Here")]
	public LootSystem ltSystem1;
	public LootSystem ltSystem2;

	[Header ("Cost of Reviving")]
	public int costRev;

	[Header ("Camera here")]
	public CameraManager camMan;

	[Header ("Attack Collider here")]
	public BoxCollider2D atkColi;

	[Header ("Audio Source for walk here")]
	public AudioSource audSouWalk;
	
	[Header ("Game Manager Object Here")]
	public GameManager gameMan;
	public UIManager uiMan;

	//should this be GameObject?
	[Header ("PlayerChar Prefab")]
	public Transform playerPrefab;

	public GameObject DmgIndPrefab;

	//player's position, mostly so we don't do new Vector every time
	//ask if this is more effcient way to do it
	//private Vector3 playerPos;

	//isGrounded: is player touching the ground
	private bool isGrounded = true, canMove = true;
	private Rigidbody2D playerRigidbody;
    private int health, stamina;
	private Vector2 knockDir = new Vector2 (1f, 1f);
	private float curAtkTime, lastYPos;

	private bool P1_Dead, P2_Dead;

	private bool blockActive;

	private Vector3 deadPos;
	private float snareDur;
	private Animator animator;

	//checking if player is attacking or not
	private bool attacking;

	private float timerHold1;
	private float timerHold2;



	void Start()
	{	
		//connecting the prefabs
		GameObject gameManObject = GameObject.FindGameObjectWithTag ("GameManager");
		gameMan = gameManObject.GetComponent<GameManager> ();
		uiMan = gameManObject.GetComponent<UIManager> ();

		GameObject cameraObject = GameObject.FindGameObjectWithTag ("MainCamera");
		camMan = cameraObject.GetComponent <CameraManager> ();

		GameObject lootP1 = GameObject.FindGameObjectWithTag ("LootP1");
		ltSystem1 = lootP1.GetComponent<LootSystem>();
		GameObject lootP2 = GameObject.FindGameObjectWithTag ("LootP2");
		ltSystem2 = lootP2.GetComponent<LootSystem>();

		//GameObject respawnManObject = GameObject.FindGameObjectWithTag ("RespawnManager");
		//respawnMan = respawnManObject.GetComponent<RespawnManager> ();


		//playerPos = transform.position
		//or maybe not, we need to update the position every time, I'll try this later
		playerRigidbody = this.GetComponent<Rigidbody2D> ();
		stamina = maxStamina;
		health = maxHealth;

		animator = this.GetComponent<Animator> ();

		
		PlayerPrefs.SetInt("deadStateP1",0);
		PlayerPrefs.SetInt("deadStateP2",0);

		lastYPos = transform.position.y;
	}

	// Update is called once per frame
	void Update () 
	{
		Debug.Log ("1");
		if(Input.GetKeyDown(KeyCode.P)){
            PlayerPrefs.DeleteAll();
			Application.LoadLevel("Start");
		}

		if (Input.GetButtonDown("P1_Revive") && P1_Dead) {
			if(playerNumber == 1){

				if(ltSystem1.getLoot() >= costRev){
					Debug.Log("testing");
				health = maxHealth;
				canMove = true;
				P1_Dead = false;
					ltSystem1.redLoot(costRev);
				uiMan.healthP1Ratio((float)health/(float)maxHealth);
					costRev += 2;
				//JUSTIN PUT ANIMATION REVIVE HERE
				animator.SetBool("isDead", false);
				}
			}
		}
		if (Input.GetButtonDown("P1_Revive") && P2_Dead) {
			if(playerNumber == 2){
				if(ltSystem2.getLoot() >= costRev){
				health = maxHealth;
				canMove = true;
				P2_Dead = false;
					ltSystem2.redLoot(costRev);
				uiMan.healthP2Ratio((float)health/(float)maxHealth);
					costRev += 2;
					//animatiob
				animator.SetBool("isDead", false);
				}
			}
		}
		if (!canMove && snareDur > 0) {
			snareDur -= Time.deltaTime;
			if(snareDur<0){
				canMove = true;
				animator.SetBool("isSnared", false);
			}
		}
		if (P1_Dead && P2_Dead) {
			Application.LoadLevel("Lose");
		}

		if (playerNumber == 1 && canMove)
		{
			// do all player 1 inputs

			// revive

			//left right
			//turning, does scale -1 cause any problem? if not, we'll just do that
			if (Input.GetAxis("P1_Horizontal") != 0)
			{
				MoveHorizontal("P1_Horizontal");

				animator.SetBool ("isWalking", true);
				audSouWalk.enabled = true;
			}
			else
			{
				if (animator.GetBool ("isWalking") == true)
					animator.SetBool ("isWalking", false);
				if (audSouWalk.enabled)
					audSouWalk.enabled = false;
			}

			//jump
			if (Input.GetButtonDown("P1_Jump") && isGrounded)
			{
				Jump();
			}

		}
		else if (playerNumber == 2 && canMove && !P2_Dead)
		{
			// do player 2 movements
			Debug.Log ("2");
			if(isGrounded){
				if (Input.GetButtonDown("P2_Jump"))
				{
					
					Jump();
				}
			}
			//left right
			if (Input.GetAxis("P2_Horizontal") != 0)
			{
				MoveHorizontal("P2_Horizontal");
				animator.SetBool ("isWalking", true);
				audSouWalk.enabled = true;
			}
			else
			{
				if (animator.GetBool ("isWalking") == true)
					animator.SetBool ("isWalking", false);
				if (audSouWalk.enabled)
					audSouWalk.enabled = false;
			}


			//jump


			if (Input.GetButtonUp("P2_Block"))
			{
				
				blockActive = false;
				canMove = true;
				Debug.Log(canMove + " " + blockActive);
				animator.SetBool("isGuarding", false);
			}
		}
		if (playerNumber == 1) {
			if (Input.GetButtonDown ("P1_Block") && isGrounded && !P1_Dead) {
				int temp = stamina - cost;
				if (temp >= 0) {
					blockActive = true;
					animator.SetBool ("isGuarding", true);
					stamina -= cost;
					uiMan.staminaP1Ratio ((float)stamina / (float)maxStamina);
					canMove = false;
				} else {
					blockActive = false;
				}
			}
			if (Input.GetButtonUp ("P1_Block")) {
			
				blockActive = false;
				canMove = true;
				Debug.Log (canMove + " " + blockActive);
				animator.SetBool ("isGuarding", false);
			}
		}
	
		if (playerNumber == 2) {
			if (Input.GetButtonDown ("P2_Block")) {
				int temp2 = stamina - cost;
				if (temp2 >= 0) {
					blockActive = true;
					animator.SetBool ("isGuarding", true);
					stamina -= cost;
					uiMan.staminaP2Ratio ((float)stamina / (float)maxStamina);
					canMove = false;
				} else {
					blockActive = false;
				}
			}
				if (Input.GetButtonUp ("P2_Block")) {
					
					blockActive = false;
					canMove = true;
					Debug.Log (canMove + " " + blockActive);
					animator.SetBool ("isGuarding", false);
				}
			}

		camMan.SetPlayerPos (playerNumber, transform.position);
		//JumpAnimCheck ();
		}

		//send player position


        
	//jump animation check
	private void JumpAnimCheck()
	{
		//take last position
		if (lastYPos > transform.position.y && !isGrounded)
		{
			animator.SetBool("moveDown", false);
			animator.SetBool("moveDown", true);
		}
		
		if (isGrounded)
		{
			animator.SetBool("moveDown", false);
		}

		lastYPos = transform.position.y;
	}
	
	/*
	private void DashHorizontal (string playerNum)
	{
		transform.position += new Vector3 (Input.GetAxis(playerNum) * sprint * Time.deltaTime, 0, 0);
	}*/

	private void MoveHorizontal (string playerNum)
	{
		//turning
		if (Input.GetAxis(playerNum) > 0)
		{
			transform.localScale = new Vector3(0.3f, 0.3f, 1); //need to make the value based off the object's original
		}
		else
		{
			transform.localScale = new Vector3(-0.3f, 0.3f, 1);
		}

		//left right
		transform.position += new Vector3 (Input.GetAxis(playerNum) * speed * Time.deltaTime, 0, 0);

		//animation stuff here
	}
	
	private void Jump ()
	{

		//jump, move character up
		playerRigidbody.AddForce (Vector2.up * jumpStr);

		//because player is mid air || to prevent player from jumoing until they touch the ground again
		isGrounded = false;

		//animation stuff here
		animator.SetTrigger ("moveUp");
	}

	//calculates attack damage player deals & sends
	public int AtkCalc(int playerNum)
	{
		if (playerNum == 1) {
			Debug.Log((int)((float)Random.Range (minDamage, maxDamage + 1) * (timerHold1 / 1.5f)));
			return (int)((float)Random.Range (minDamage, maxDamage + 1) * (timerHold1 / 1.5f));
		} else if (playerNum == 2) {
			return (int)((float)Random.Range (minDamage, maxDamage + 1) * (timerHold2 / 1.5f));
		} else {
			return 0;
		}
	}

	//take damage no knockback
	public void TakeDmg(int dmg){

		//change animation state
		animator.SetTrigger ("isFlinch");

		if (blockActive) {
					dmg = 0;
					InitDmg("Block");
		}
		health -= dmg;
		InitDmg (dmg.ToString ());
		if (playerNumber == 1) {
			uiMan.healthP1Ratio((float)health/(float)maxHealth);
			if (health <= 0) 
			{
				//JUSTIN PUT THE DEAD ANIMATION HERE
				animator.SetBool("isDead", true);

				deadPos = new Vector3(this.transform.position.x , this.transform.position.y, this.transform.position.z);
				P1_Dead = true;
				//gameObject.SetActive(false);
				canMove = false;
				PlayerPrefs.SetInt("deadStateP1",1);
				//respawnMan.setDeadState(1,true,deadPos);
				Debug.Log("Player 1 died");
			

			}
		}
		if (playerNumber == 2) {
			uiMan.healthP2Ratio((float)health/(float)maxHealth);
			if (health <= 0) {
				//JUSTIN PUT THE DEAD ANIMATION HERE
				animator.SetBool("isDead", true);

				deadPos = new Vector3(this.transform.position.x , this.transform.position.y, this.transform.position.z);
				P2_Dead = true;
					//gameObject.SetActive(false);
					PlayerPrefs.SetInt("deadStateP2",1);
					canMove = false;
					//respawnMan.setDeadState(2,true,deadPos);
					//Debug.Log("Player 2 died");
				//Destroy (gameObject);
				}
		}
	}
	//does taking damage & knock back
	public void TakeDmg(Vector3 enemyPos, int dmg, float force)
	{
		if (enemyPos.x > this.transform.position.x)
		{
			knockDir.x = -1f;
		}
		else if (enemyPos.x < this.transform.position.x)
		{
			knockDir.x = 1f;
		}

		//change animation state
		animator.SetTrigger ("isFlinch");

		health -= dmg;
		InitDmg (dmg.ToString ());
		if (playerNumber == 1) {
			uiMan.healthP1Ratio((float)health/(float)maxHealth);
			if (health <= 0) {
				//PUT DEAD ANIMATION HERE
				P1_Dead = true;
				deadPos = new Vector3(this.transform.position.x , this.transform.position.y, this.transform.position.z);
				canMove = false;
				PlayerPrefs.SetInt("deadStateP1",1);
				//respawnMan.setDeadState(1,true,deadPos);
				Debug.Log("Player 1 died");
				//Destroy (gameObject);
			}
		}
		if (playerNumber == 2) {
			uiMan.healthP2Ratio((float)health/(float)maxHealth);
			if (health <= 0) {
				//PUT THE DEAD ANIMATION HERE
				P2_Dead = true;
				deadPos = new Vector3(this.transform.position.x , this.transform.position.y, this.transform.position.z);
				canMove = false;	
				PlayerPrefs.SetInt("deadStateP2",1);
				//respawnMan.setDeadState(2,true,deadPos);
				Debug.Log("Player 2 died");
				//Destroy (gameObject);

			}
		}
		playerRigidbody.AddForce (knockDir * force);
	}


	void OnCollisionEnter2D (Collision2D other)
	{
		//check if player's standing on ground
		//currently, this will also trigger if player's hugging wall & also on ceiling... I'll fix that later...
		if (other.transform.tag == "Ground")
		{
			isGrounded = true;

		}
	}

	//getting animator commend from attack script
	public void charAnimAttack(string state)
	{
		switch(state)
		{
		case("charge"):
			animator.SetBool ("isWalking", false);
			animator.SetBool ("isCharging", true);
			break;
		case("attacking"):
			animator.SetBool ("isCharging", false);
			animator.SetBool ("isAttacking", true);
			break;
		default:
			animator.SetBool ("isAttacking", false);
			break;
		}
	}

	//setting canMove
	public void setMove(bool move){
		canMove = move;
	}

	void debugLog(){
		Debug.Log (PlayerPrefs.GetInt("deadStateP1"));
		Debug.Log (PlayerPrefs.GetInt("deadStateP2"));
	}

	public void addStam(int stam, int player){
		int temp = stamina + stam;
        if (temp > 100)
        {
            stamina = 100;
        }
        else
        {
            stamina += stam;
            if (player == 1)
            {
                uiMan.staminaP1Ratio((float)stamina / (float)maxStamina);
            }
            else if (player == 2)
            {
                uiMan.staminaP2Ratio((float)stamina / (float)maxStamina);
            }
        }
	}

	public void Snare(float duration){
		InitDmg ("snared");
		canMove = false;
		snareDur = duration;
		animator.SetBool ("isSnared", true);
	}

	void InitDmg(string text){
		GameObject temp = Instantiate (DmgIndPrefab) as GameObject;
		RectTransform tempRect = temp.GetComponent<RectTransform> ();
		if (playerNumber == 1) {

			temp.transform.SetParent (transform.FindChild ("Canv_P1"));
			tempRect.transform.localPosition = DmgIndPrefab.transform.localPosition;
			tempRect.transform.localScale = DmgIndPrefab.transform.localScale;
			tempRect.transform.localRotation = DmgIndPrefab.transform.localRotation;
		
			temp.GetComponent<Text> ().text = text;
			temp.GetComponent<Animator> ().SetTrigger ("Hit");
			Destroy (temp.gameObject, 1.5f);
		} else if (playerNumber == 2) {
			temp.transform.SetParent (transform.FindChild ("Canv_P2"));
			tempRect.transform.localPosition = DmgIndPrefab.transform.localPosition;
			tempRect.transform.localScale = DmgIndPrefab.transform.localScale;
			tempRect.transform.localRotation = DmgIndPrefab.transform.localRotation;
			
			temp.GetComponent<Text> ().text = text;
			temp.GetComponent<Animator> ().SetTrigger ("Hit");
			Destroy (temp.gameObject, 1.5f);
		}
	}

	public void setCharge(int playerNum, float timer){
		if(playerNum == 1){
			timerHold1 = timer;
		}
		if (playerNumber == 2) {
			timerHold2 = timer;
		}
	}
	public bool deadState(int playerNum){
		if (playerNum == 1) {
			return P1_Dead;
		} else if (playerNum == 2) {
			return P2_Dead;
		} else {
			return false;
		}

	}

}
