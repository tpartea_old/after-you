﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StaminaBar : MonoBehaviour {
	public float maxStam;
	public float currentStam;
	public int stamRec; //stamina recovery


	// Use this for initialization
	void Start(){
		InvokeRepeating("addStam", 1, 1);
	}
	// Update is called once per frame
	void Update () {
		
		Image Image = gameObject.GetComponent<Image> ();
		Image.fillAmount = (float)(currentStam / maxStam) ;
	}

	void addStam(){
		currentStam += stamRec;
		if (currentStam > maxStam) {
			currentStam = maxStam;
		}
	}
	public float checkStam(){
		return currentStam;	
	}

	public float useStam(float amount){
		currentStam -= amount;
		return currentStam;
	}
}
