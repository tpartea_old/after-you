﻿using UnityEngine;
using System.Collections;

public class gameInfoScript : MonoBehaviour {
	public GameObject a1;
	public GameObject a2;
	public GameObject a3;
	public GameObject a4;
	public GameObject a5;


	private int clicked;
	// Use this for initialization
	void Start () {
		clicked = 0;
		a2.SetActive (false);
		a3.SetActive (false);
		a4.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("P1_OK/Attack")) {
			clicked += 1;
		}
		switch (clicked) {
		case 0: a1.SetActive(true);break;
		case 1: a2.SetActive(true);a1.SetActive(false);break;
		case 2: a3.SetActive(true);a2.SetActive(false);break;
		case 3: a4.SetActive(true);a3.SetActive(false); a5.SetActive(false);break;
		}
	}
}
