﻿using UnityEngine;
using System.Collections;

public class FallCollider : MonoBehaviour {
	private bool player1;
	private bool player2;

    [Header ("groundless sprite here")]
    public Sprite floLess;

	public Collider2D collider;
    public SpriteRenderer spriRend;

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player1") {
			player1 = true;
			Debug.Log ("Player1 enter");
		} else if (other.tag == "Player2") {
			player2 = true;
			Debug.Log ("Player2 enter");
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player1") {
			player1 = false;
			Debug.Log ("Player1 leave");
		} else if (other.tag == "Player2") {
			player2 = false;
			Debug.Log ("Player2 leave");
		}
	}

	void Update(){
		if (player1 && player2) {
			collider.enabled = false;
            spriRend.sprite = floLess;
			Debug.Log ("collider disabled");
		}
	}
}
