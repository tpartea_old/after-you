﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(BoxCollider2D))]

public class CheckPlayer : MonoBehaviour {

	[Header ("Requires both player in trigger box to activate")]

	[Header ("Activate on contact")]
	public GameObject[] activateObjs;

	private bool p1Check = false, p2Check = false;

	void Update()
	{
		//activates when both player is inside the trigger box
		if (p1Check && p2Check)
			ActivateObjs ();
	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player1")
			p1Check = true;
		else if (other.tag == "Player2")
			p2Check = true;
	}

	//so it only activate when both player is inside the trigger box
	public void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player1")
			p1Check = false;
		else if (other.tag == "Player2")
			p2Check = false;
	}

	//this activates stuff in array
	//could have inherited from WakeOnTrig?
	//does this work the same as Java?
	private void ActivateObjs()
	{
		for (int i = 0; i < activateObjs.Length; i ++) 
		{
			activateObjs[i].SetActive(true);
		}
	}
}
