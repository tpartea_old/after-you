﻿using UnityEngine;
using System.Collections;

public class trap2 : MonoBehaviour {
	public int damage;
	public float snareDuration;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player1" || other.tag == "Player2") {
			other.SendMessageUpwards("TakeDmg", damage);
			other.SendMessageUpwards("Snare",snareDuration);
		}
		if (other.tag == "Ground") {
			Destroy(gameObject);
		}
	}
}
