﻿using UnityEngine;
using System.Collections;

public class WinningCollider : MonoBehaviour {

	public GameManager GM;
	public LootSystem p1;
	public LootSystem p2;
	public LootSystem total;

	void OnTriggerEnter2D (Collider2D other)
	{
		
		if (other.tag == "Player1" || other.tag == "Player2")
		{
			PlayerPrefs.SetInt("P1Loot", p1.getLoot());
			PlayerPrefs.SetInt ("P2Loot", p2.getLoot());
			PlayerPrefs.SetInt("TotalLoot", total.getLoot());
			PlayerPrefs.SetFloat("RatioP1", GM.calculateRatioP1());
			PlayerPrefs.SetFloat("RatioP2", GM.calculateRatioP2());
			Application.LoadLevel ("Win");

		}
	}
}
