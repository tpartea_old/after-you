﻿using UnityEngine;
using System.Collections;

public class RandomTrapGenerator : MonoBehaviour {
	public int maxTrap;
	public float heightDist;
	public CharmScript cs;
	public GameObject trap1PreFab;
	public GameObject trap2PreFab;

	private float randomX;

	// Use this for initialization
	void Start () {
		randomX = Random.Range (0, 4);
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player1" || other.tag == "Player2") {
			int randomGen = Random.Range (1,maxTrap);
			Vector3 trapLoc1 = new Vector3(this.transform.position.x + randomX, this.transform.position.y + heightDist, this.transform.position.z);
			switch(randomGen){
			case 0:
			case 1:
				Instantiate (trap1PreFab, trapLoc1, Quaternion.identity);
				cs.broke();
				Destroy (gameObject);
				break;
			case 2:
				Instantiate (trap2PreFab, trapLoc1, Quaternion.identity);
				cs.broke();
				Destroy (gameObject);
				break;
			}
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
