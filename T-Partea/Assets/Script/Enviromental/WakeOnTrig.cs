﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(BoxCollider2D))]

public class WakeOnTrig : MonoBehaviour {

	[Header ("Activate on contact")]
	public GameObject[] enemyObjs;

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player1" || other.tag == "Player2")
		{
			ActivateObjs();
		}
	}

    //this activates stuff in array
	private void ActivateObjs()
	{
		for (int i = 0; i < enemyObjs.Length; i ++) 
		{
			enemyObjs[i].SetActive(true);
		}
	}
}
