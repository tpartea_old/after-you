﻿using UnityEngine;
using System.Collections;

public class CharmScript : MonoBehaviour {
	private Animator animator;
	// Use this for initialization
	void Start () {
		animator = this.GetComponent<Animator> ();
	}
	
	public void broke(){
		animator.SetBool ("Opened", true);
	}
}
