﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BoxCollider2D))]

public class Teleport : MonoBehaviour {

	[Space(5f)]
	[Header ("This will be added to player position")]
	public Vector2 playerOffSet;

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Player1" || other.tag == "Player2")
		{
			other.transform.position = new Vector3(other.transform.position.x + playerOffSet.x, other.transform.position.y + playerOffSet.y, other.transform.position.z);
		}
	}
}
