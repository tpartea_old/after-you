﻿using UnityEngine;
using System.Collections;

public class Trap1 : MonoBehaviour {
	public int damage;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player1" || other.tag == "Player2") {
			other.SendMessageUpwards("TakeDmg", damage);
		}
		if (other.tag == "Ground") {
			Destroy(gameObject);
		}
	}

}
