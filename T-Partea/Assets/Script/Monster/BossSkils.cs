﻿using UnityEngine;
using System.Collections;

public class BossSkils : MonoBehaviour {
	public MonsterBase Boss;
	public float xIncrease;
	public GameObject shockWave1;
	public GameObject shockWave2;
	public GameObject shockWave3;

	public float interval;
	private float timer;

	public GameObject bottlePrefab;

	public GameObject totem;

	public GameObject bottle;

	private int bossMode;
	private int i ;

	private float timerShockWave;
	private bool skillON;
	private Vector3 locationSpawn;

	private ShockWaveScript shockWave01Script;
	private ShockWaveScript shockWave02Script;
	private ShockWaveScript shockWave03Script;

	private Animator anim;

	public float xPos;
	public float yPos;

	private bool charmOut;
	// Use this for initialization
	void Start () {
		locationSpawn = Boss.getLocation ();
		bossMode = 1;
		timer = interval;
		shockWave01Script = shockWave1.GetComponent<ShockWaveScript> ();
		shockWave02Script = shockWave2.GetComponent<ShockWaveScript> ();
		shockWave03Script = shockWave3.GetComponent<ShockWaveScript> ();

		anim = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (skillON) {
			anim.SetTrigger("Shock");
			//delay for animation
			/*
			timerShockWave = 1;
			while (timerShockWave > 0)
				timerShockWave -= Time.deltaTime;
			timerShockWave = 0;*/

			timerShockWave += Time.deltaTime;
			if(timerShockWave >=0){
				shockWave01Script.changeState();
				shockWave1.SetActive(true);
				shockWave2.SetActive(false);
				if(timerShockWave >= 1.5){
					shockWave02Script.changeState();
					shockWave1.SetActive(false);
					shockWave2.SetActive(true);
					if(timerShockWave >= 3){
						shockWave03Script.changeState();
						shockWave3.SetActive(true);
						shockWave2.SetActive(false);
						if(timerShockWave >= 4.5){
							shockWave3.SetActive(false);
							timerShockWave = 0;
							skillON = false;
						}
					}
				}
			}
		}
		if (Boss.getHealthRatio() < 0.8f) {
			bossMode = 2;
		}
		if (Boss.getHealthRatio() < 0.5f) {
			bossMode = 3;
		}
		if (timer <= 0 && !skillON) {
			i = Random.Range (1, bossMode + 1);
			if (i == 1) {
				skillON = true;
				timer = interval;
			} else if (i == 2) {
				throwBottle ();
				timer = interval;

				//all of the above is working just fine
			} else if (i == 3 && !charmOut) {
				//put the shield and charm here
				charmOut = true;
				Boss.setImmune(true);
				createTotem ();
				timer = interval;
			}
		}
	}

	void throwBottle(){
		Debug.Log ("SHOOT");
		Instantiate (bottlePrefab,locationSpawn,Quaternion.identity);
	}

	void shockWaveActivate(){
		shockWave1.SetActive (true);
		shockWave2.SetActive (true);
		shockWave3.SetActive (true);
	}

	void createTotem(){
		//totem is the charm that needs to be destroyed!
		Instantiate(totem,new Vector3(xPos, yPos, this.transform.position.z),Quaternion.identity);
	}

	public void setCharm(){
		charmOut = false;
	}
}
