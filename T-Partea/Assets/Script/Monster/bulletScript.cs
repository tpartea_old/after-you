﻿using UnityEngine;
using System.Collections;

public class bulletScript : MonoBehaviour {
	public float speed;
	public float timeBeforeDestruction;
	public int damage;

	//recieve player position
    public Vector3 tarPos;
	
	// Use this for initialization
	void Start () 
    {
		//set target
		int tempInt;
		tempInt = Random.Range (1, 3);
		switch(tempInt)
		{
		case(1):
			tarPos = GameObject.FindGameObjectWithTag("Player1").transform.position;
			break;
		case(2):
			tarPos = GameObject.FindGameObjectWithTag("Player2").transform.position;
			break;
		}

		transform.rotation = Quaternion.FromToRotation (Vector3.up, tarPos);
	}
	
	// Update is called once per frame
	void Update () {
		/*
		timeBeforeDestruction -= Time.deltaTime;
		if (timeBeforeDestruction < 0) {
			Destroy(this.gameObject);
		}

		Vector3 pos = transform.position;

		Vector3 velocity = Vector3.forward;
		pos += transform.rotation * velocity;

		transform.position = pos;
		*/

        //use move towards here
		transform.position = Vector3.MoveTowards (transform.position, tarPos, speed * Time.deltaTime);
		Debug.Log ("bullet moving" + tarPos);

		//delete when reached target
		if (transform.position == tarPos)
			Destroy (this.gameObject);
	}

	public void setTarget(Vector3 playerPos)
	{
		tarPos = playerPos;
		Debug.Log ("player position set" + tarPos);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log("ontriggerenter activated");
		if (other.isTrigger != true) 
		{
			if(other.CompareTag("Player1") || other.CompareTag("Player2")){
				other.SendMessageUpwards("TakeDmg", damage);
				Destroy (gameObject);
			}
			
		}
	}
}
