﻿using UnityEngine;
using System.Collections;

public class BottleMonster : MonoBehaviour {
	public GameObject bottlePrefab;
	public float FireDelay;
	public MonsterBase monster;

	private float timer;
	private Vector3 locationSpawn;
	private Animator anim;
	// Use this for initialization
	void Start () {
		timer = FireDelay;
		locationSpawn = monster.getLocation ();
		anim = this.gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;

		//play animation
		if (timer <= 0.1f)
		{
			anim.SetTrigger("isCharging");
		}

		if (timer <= 0) {
			anim.SetTrigger("isAttacking");
			Instantiate(bottlePrefab,locationSpawn,Quaternion.identity);
			timer = FireDelay;
		}
	}
}
