﻿using UnityEngine;
using System.Collections;

public class PoisonFog : MonoBehaviour {
	public float fireDelay;
	float coolDownTimer = 0;
	public int damage;

	void OnTriggerStay2D(Collider2D other){
		coolDownTimer -= Time.deltaTime;
		if (coolDownTimer < 0) {
			coolDownTimer = fireDelay;
			other.SendMessageUpwards("TakeDmg",damage);
		}
	}
}
