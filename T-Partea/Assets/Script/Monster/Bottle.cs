﻿using UnityEngine;
using System.Collections;

public class Bottle : MonoBehaviour {
	public Rigidbody2D RB;

	public int damage;

	public int distanceMin;
	public int distanceMax;
	public int height;
	public int heightMax;

	private bool shoot;
	// Use this for initialization
	void Start () {

			RB.AddForce(new Vector3(Random.Range (distanceMin, distanceMax),Random.Range (height,heightMax),0));

	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player1" || other.tag == "Player2") {
			other.SendMessageUpwards ("TakeDmg", damage);
			Destroy (gameObject);
		} else if(other.tag == "Ground") {
			Destroy(gameObject);
		}
	}
}
