﻿using UnityEngine;
using System.Collections;

public class RangeMonster : MonoBehaviour {
	public GameObject bullet;
	public float fireDelay;
	public float projectileSpeed;
	float coolDownTimer = 0;
	Transform firePoint;
	// Use this for initialization
	void Start () {
		firePoint = transform.FindChild ("firePoint");
		if (firePoint == null) {
			Debug.Log ("fire point missing");
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (coolDownTimer -= Time.deltaTime);
	}
	

	void OnTriggerStay2D(Collider2D other){
		coolDownTimer -= Time.deltaTime;

		//start up animation early


		if (coolDownTimer < 0) {
			Debug.Log ("timerUp");
			coolDownTimer = fireDelay;
			if (other.tag == "Player1" || other.tag == "Player2") {
				/*
				Vector3 direction = other.transform.position - transform.position;
                //set rotation
                Quaternion bulRot = Quaternion.FromToRotation (Vector3.up, other.transform.position);
				Debug.Log (other.transform.position);
				*/

				//play through animation


				Transform bulletAttack = Instantiate (bullet, transform.position, transform.rotation) as Transform;
                //get bullet script from the instantiated oject & send target position
				//bulletAttack.GetComponent<bulletScript>().tarPos = other.transform.position;
				Debug.Log ("shooting");
			}
		}
	}
}