﻿using UnityEngine;
using System.Collections;

public class AOEMonster : MonoBehaviour {
	public GameObject poison1;
	public GameObject poison2;
	public GameObject poison3;
	public GameObject poison4;

	public float timeIntervals;

	private float timer;
	private int curState;
	// Use this for initialization
	void Start () {
		timer = timeIntervals;
		curState = 1;

		poison1.SetActive (false);
		poison2.SetActive (false);
		poison3.SetActive (false);
		poison4.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (timer <= 0) {
			switch(curState){
			case 0: break;
			case 1: curState += 1;
				timer = timeIntervals;
				poison3.SetActive(true);
				poison1.SetActive(true);
				break;
			case 2: curState += 1;
				timer = timeIntervals;
				poison1.SetActive(false);
				poison3.SetActive(false);
				poison2.SetActive(true);
				poison4.SetActive(true);
				break;
			case 3: curState += 1;
				timer = timeIntervals;
				poison2.SetActive(false);
				poison3.SetActive(true);
				break;
			case 4:curState += 1;
				
				timer = timeIntervals;
				poison3.SetActive(false);
				poison4.SetActive(false);
				poison2.SetActive(true);
				poison1.SetActive(true);
				break;
			case 5: curState = 1;
				timer = timeIntervals;
				poison1.SetActive(false);
				poison2.SetActive(false);
				poison3.SetActive(false);
				poison4.SetActive(false);
				break;
			}
		}
	}
	 
}
