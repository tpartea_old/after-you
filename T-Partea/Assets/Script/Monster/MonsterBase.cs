using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MonsterBase : MonoBehaviour {

	//public GameManager lootMan;
	[Header ("Damage Indicator prefab")]
	public GameObject DmgIndPrefab;

    [Header ("Monster Stat")]
    public int MaxHealth;
	public int damage;
	public float knockBackStr = 5;

    [Header ("Amount of loot this monster drops")]
    public int loot;

	[Header ("Manager Objects here")]
	public GameManager gameMan;

	[Header ("Insert Loots")]
	public Transform lootx1;

	[Header("Image HPBar")]
	public Image hpBar;
    private int health;
	//private GameManager gameMan;
	//private UIManager UIMan;

	private Animator animate;
	//check who attacks
	private int playerNum;

	private bool immune;
	public bool Charm;

	// Use this for initialization
	void Start () 
    {
		GameObject gameManagerObj = GameObject.FindGameObjectWithTag ("GameManager");
		gameMan = gameManagerObj.GetComponent<GameManager>();
		health = MaxHealth;
		//UIMan = gameManagerObj.GetComponent<UIManager>();
		animate = this.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
    {
		hpBar.fillAmount = ((float)health / (float)MaxHealth);
	    

		/*
	     * if health is 0 or less, play death animation
	     * when death animation ends(or while it's ending?) spawn loot bag
	     * afterward destroy object
	     */


	}

	public void playerNumSet(int num){
		playerNum = num;
	}

	//calculating damage taken
	private void TakeDamage(Transform atkPlayer)
	{	if (!immune) {
			PlayerController playerControl = atkPlayer.transform.GetComponentInParent<PlayerController> ();

			//storing it incase I want to show it on UI
			int damageTaken = playerControl.AtkCalc (playerNum);
			Debug.Log ("MonsterAttacked");
			Debug.Log (playerNum);
			animate.SetTrigger ("Attacked");
			InitDmg (damageTaken.ToString ());
			health -= damageTaken;

			if (playerNum == 1) {
				gameMan.addP1 (damageTaken);
				playerNum = 0; //returning it to 0
			}
			if (playerNum == 2) {
				gameMan.addP2 (damageTaken);
				playerNum = 0; //returning it to 0
			}
			//adds damage dealt to game manager
			if (health <= 0) {
				if (Charm) {
					Debug.Log("Boss Death");
					GameObject boss = GameObject.FindGameObjectWithTag ("Boss");
					MonsterBase BossScript = boss.GetComponent<MonsterBase> ();
					BossScript.setImmune (false);
					BossSkils bossSkill = boss.GetComponent<BossSkils> ();
					bossSkill.setCharm();
					//set animation for not immune
					Destroy (gameObject);
				}
				if (loot > 0) {
					Vector3 lootLocation = new Vector3 (gameObject.transform.position.x + 0.1f, gameObject.transform.position.y, gameObject.transform.position.z);
					Destroy (gameObject);
					Debug.Log ("Destroyed");
					for (int i = 0; i < loot; i++) {
						Instantiate (lootx1, lootLocation, Quaternion.identity);
					}
				} else {
					Destroy (gameObject);
					Debug.Log ("Destroyed");
				}
			
			}
			Debug.Log ("Attcked");
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		/*
		if (other.transform.tag == "Attack_P1")
		{
			Transform player = other.transform;

			TakeDamage(player);
			Debug.Log("p1 got hit");
		}
		else if (other.transform.tag == "Attack_P2")
		{
			Transform player = other.transform;
			
			TakeDamage(player);
			Debug.Log("p2 got hit")
		}
		*/

		if (other.transform.tag == "Player1")
		{
			//play animation
			animate.SetTrigger("isAttacking");

			PlayerController playControl = other.transform.GetComponent<PlayerController>();
			playControl.TakeDmg(this.transform.position, damage, knockBackStr);
			Debug.Log("p1 got hit");
		}

		if (other.transform.tag == "Player2")
		{
			//play animation
			animate.SetTrigger("isAttacking");

			PlayerController playControl = other.transform.GetComponent<PlayerController>();
			playControl.TakeDmg(this.transform.position, damage, knockBackStr);
			Debug.Log("p2 got hit");
		}
	}

	void InitDmg(string text){
		GameObject temp = Instantiate (DmgIndPrefab) as GameObject;
		RectTransform tempRect = temp.GetComponent<RectTransform> ();
		temp.transform.SetParent (transform.FindChild ("EnemyCanvas"));
		tempRect.transform.localPosition = DmgIndPrefab.transform.localPosition;
		tempRect.transform.localScale = DmgIndPrefab.transform.localScale;
		tempRect.transform.localRotation = DmgIndPrefab.transform.localRotation;

		temp.GetComponent<Text> ().text = text;
		temp.GetComponent<Animator>().SetTrigger("Hit");
		Destroy (temp.gameObject, 1.5f);
	}

	public Vector3 getLocation(){
		return new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z);
	}
	public float getHealthRatio(){
		return ((float)health / (float)MaxHealth);
	}

	public void setImmune(bool imunity){
		immune = imunity;
	}
}

