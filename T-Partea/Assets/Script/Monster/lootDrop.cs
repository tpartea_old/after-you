﻿using UnityEngine;
using System.Collections;

public class lootDrop : MonoBehaviour {
	[Header("lootSystems")]
	public LootSystem p1Loot;
	public LootSystem p2Loot;
	public LootSystem totalLoot;

	[Header("Amount of loot receive")]
	public int individualLoot;
	public int totalLootIncrease;

	private float destroyTimer = 1;
	private bool willDestroy = false;
	private AudioSource audioSource;

	void Start () {

		p1Loot = GameObject.Find ("Player1Loot").GetComponent (typeof(LootSystem)) as LootSystem;
		p2Loot = GameObject.Find ("Player2Loot").GetComponent (typeof(LootSystem)) as LootSystem;
		totalLoot = GameObject.Find ("TotalLoot").GetComponent (typeof(LootSystem)) as LootSystem;
		audioSource = GetComponent<AudioSource> ();
	}

	void Update()
	{
		if (willDestroy)
		{
			destroyTimer -= Time.deltaTime;
			if (destroyTimer < 0)
				Destroy(this.gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player1"){
			Debug.Log ("player 1 gets");
			p1Loot.addLoot(individualLoot);
			totalLoot.addLoot(totalLootIncrease);
			willDestroy = true;
			audioSource.Play();
			//doing this cuz lazy, don't do this on other teams
			GetComponent<SpriteRenderer>().enabled = false;
		}
		if (other.tag == "Player2"){
			Debug.Log ("player 2 gets");
			p2Loot.addLoot(individualLoot);
			totalLoot.addLoot(totalLootIncrease);
			willDestroy = true;
			audioSource.Play();
			//doing this cuz lazy, don't do this on other teams
			GetComponent<SpriteRenderer>().enabled = false;
		}
	}
}
