﻿using UnityEngine;
using System.Collections;
public class MonsterMove : MonoBehaviour
{
    [Header("Distance to keep")] //improve this to min & max idle distance
    public float idleDis;
    public float engageDis;
    [Header("Engage timer")]
    public float minEngaTim;
    public float maxEngaTim;
    [Header("Speed, should be slow")]
    public float movSpeed;
    //current target
    private Vector3 playerPos, movDir, tarPos; //movDir = moving direction
    private Vector3 startScale, curScale; //vector3 used for scale of object
    private GameObject player01, player02;
    private float tempFloat, curDis, curTimer;
    private bool canMove;
	private Animator anim;

    void Start()
    {
        //initiate vector 3
        movDir = new Vector3(0, 0, 0);
        //finds player
        player01 = GameObject.FindGameObjectWithTag("Player1");
        player02 = GameObject.FindGameObjectWithTag("Player2");
        //set distance
        curDis = idleDis;
        //set scale
        startScale = transform.localScale;
        curScale = transform.localScale;
        //initiate wait time before engaging
        curTimer = Random.Range(minEngaTim, maxEngaTim + 1);

		anim = GetComponent<Animator> ();

		canMove = true;
    }
    // Update is called once per frame
    void Update()
    {
        /*
        timer -= Time.deltaTime;
        if(timer <0){
            if (transform.position.x > 32) {
                moveLeft = true;
                moveRight = false;
                timer = timerOri;
            }
            if (transform.position.x < 32) {
                moveRight = true;
                moveLeft = false;
                timer = timerOri;
            }
        }
        if (canMove) {
            if (moveLeft) {
                transform.Translate (Vector3.left * Time.deltaTime);
            } else if (moveRight) {
                transform.Translate (Vector3.right * Time.deltaTime);
            }
        }*/
        //rewrite from here
        if (canMove)
        {
            findClosePlayer();
            findDirection();
            keepDistance();
            changeState();
        }
    }
    private void findClosePlayer()
    {
        //initialise, it's just to make if stament line shorter
        tempFloat = Vector3.Distance(transform.position, player01.transform.position);
        playerPos = player01.transform.position;
        //set target to player 2 if they're closer
        if (Vector3.Distance(transform.position, player02.transform.position) < tempFloat)
            playerPos = player02.transform.position;
    }
    //find which way player is (left or right?)
    private void findDirection()
    {
        if (playerPos.x < transform.position.x)
        {
            movDir.x = -1; //looking left, I think
            curScale.x = startScale.x;
            transform.localScale = curScale;
            if (curDis == idleDis)
                curDis = idleDis;
            else if (curDis == engageDis)
                curDis = engageDis;
        }
        else
        {
            movDir.x = 1;
            curScale.x = -(startScale.x);
            transform.localScale = curScale;
            if (curDis == idleDis)
                curDis = -idleDis;
            else if (curDis == engageDis)
                curDis = -engageDis;
        }
    }
    private void keepDistance()
    {
        //set position the enemy needs to be at
        tarPos = transform.position;
        tarPos.x = playerPos.x + curDis;

		if (tarPos != transform.position)
			anim.SetBool ("isWalking", true);
		else
			anim.SetBool ("isWalking", false);

        //move
        //Debug.Log ("enemy moving to " + tarPos + "\nDistance:" + curDis);
        transform.position = Vector3.MoveTowards(transform.position, tarPos, movSpeed * Time.deltaTime);
    }
    private void changeState()
    {
        curTimer -= Time.deltaTime;
        //change state when timer runs out
        if (curTimer < 0)
        {
            if (curDis == idleDis || curDis == -idleDis)
            {
                curDis = engageDis;
                curTimer = 5;
            }
            else if (curDis == engageDis || curDis == -engageDis)
            {
                curDis = idleDis;
                curTimer = Random.Range(minEngaTim, maxEngaTim + 1);
            }
        }
    }

	/* isn't trigger dependent any more :D
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player1" || other.tag == "Player2")
        {
            canMove = true;
        }
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        //this will break eveything in the known universe... but I'm too lazy to think of a efficient way to track it...
        if (other.tag == "Player1" || other.tag == "Player2")
            canMove = false;
    }
    */
}
