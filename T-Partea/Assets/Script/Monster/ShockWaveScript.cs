﻿using UnityEngine;
using System.Collections;

public class ShockWaveScript : MonoBehaviour {
	public int damage;
	public GameObject thisObject;

	private bool attack1;
	private bool attack2;
	public float timer;
	private float timerCur;
	// Use this for initialization
	void Start () {
		timerCur = timer;
		attack1 = false;
		attack2 = false;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player1" || other.tag == "Player2") {
			if(other.tag == "Player1" && !attack1){
				other.SendMessageUpwards("TakeDmg", damage);
				attack1 = true;
			}
			if(other.tag == "Player2" && !attack2){
				other.SendMessageUpwards("TakeDmg", damage);
				attack2 = true;
			}
		}
	}

	public void changeState(){
		attack1 = false;
		attack2 = false;
	}
}
